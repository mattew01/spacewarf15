
#include "ship.h"

Ship::Ship() : Entity()
{
	// these are your default values!
	spriteData.width = shipNS::WIDTH;
	spriteData.height = shipNS::HEIGHT;
	spriteData.x = shipNS::X;
	spriteData.y = shipNS::Y;
	spriteData.rect.bottom = shipNS::HEIGHT;
	spriteData.rect.right = shipNS::WIDTH;
	velocity.x = 0;
	velocity.y = 0;
	frameDelay = shipNS::SHIP_ANIMATION_DELAY;
	startFrame = shipNS::SHIP1_START_FRAME;
	endFrame = shipNS::SHIP1_END_FRAME;
	currentFrame = startFrame;
	radius = shipNS::WIDTH/2;
	mass = shipNS::MASS;
	collisionType = entityNS::CIRCLE;
	shieldOn = false;
	engineOn = false;
	explosionOn = false;
	direction = shipNS::NONE;
	oldAngle = 0;
	rotation = 0;
	oldX = shipNS::X;
	oldY = shipNS::Y;

	playingSound = false;
	soundTimer = 0;
}

bool Ship::initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* tm )
{
	// set up the shield
	shield.initialize( gamePtr->getGraphics(), width, height, ncols, tm);
	shield.setFrames( shipNS::SHIELD_START_FRAME, shipNS::SHIELD_END_FRAME );
	shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
	shield.setFrameDelay( shipNS::SHIELD_ANIMATION_DELAY );
	shield.setLoop( false );

	// set up the engine
	engine.initialize( gamePtr->getGraphics(), width, height, ncols, tm);
	engine.setFrames( shipNS::ENGINE_START_FRAME, shipNS::ENGINE_END_FRAME );
	engine.setCurrentFrame( shipNS::ENGINE_START_FRAME );
	engine.setFrameDelay( shipNS::ENGINE_ANIMATION_DELAY );
	engine.setLoop( true );

	// set up the explosion
	explosion.initialize( gamePtr->getGraphics(), width, height, ncols, tm);
	explosion.setFrames( shipNS::EXPLOSION_START_FRAME, shipNS::EXPLOSION_END_FRAME );
	explosion.setCurrentFrame( shipNS::EXPLOSION_START_FRAME );
	explosion.setFrameDelay( shipNS::EXPLOSION_ANIMATION_DELAY );
	explosion.setLoop( false );

	return( Entity::initialize(gamePtr, width, height, ncols, tm ) );
}

void Ship::draw()
{
	if( explosionOn ) {
		explosion.draw( spriteData );
	} else {
		Image::draw(); // draw the ship

		if( engineOn ) {
			engine.draw(spriteData);
		}
		if( shieldOn ) {
			shield.draw( spriteData, graphicsNS::ALPHA50 & colorFilter );
		}

	}
}

void Ship::update( float frameTime )
{
	if( explosionOn ) {
		explosion.update( frameTime );
		if( explosion.getAnimationComplete() )
		{
			explosionOn = false;
			visible = false;
			explosion.setAnimationComplete( false );
			explosion.setCurrentFrame( shipNS::EXPLOSION_START_FRAME );
		}
	}
	if( shieldOn )
	{
		shield.update( frameTime );
		if( shield.getAnimationComplete() )
		{
			// reset the shield
			shieldOn = false;
			shield.setAnimationComplete( false );
		}
	}
	if( engineOn )
	{
		velocity.x += (float)cos(spriteData.angle) * shipNS::SPEED * frameTime;
		velocity.y += (float)sin(spriteData.angle) * shipNS::SPEED * frameTime;
		engine.update( frameTime );
	}

	soundTimer += frameTime;
	if( soundTimer > 2.0f ) {
		soundTimer = 0;
		if( !playingSound ) {
			audio->playCue( HIT );
			playingSound = true;
		}
	}

	Entity::update( frameTime );
	oldX = spriteData.x;
	oldY = spriteData.y;
	oldAngle = spriteData.angle;

	switch( direction ) 
	{
	case shipNS::LEFT:
		rotation -= frameTime * shipNS::ROTATION_RATE;
		break;
	case shipNS::RIGHT:
		rotation += frameTime * shipNS::ROTATION_RATE;
		break;

	default:
		break;
	}
	spriteData.angle += frameTime * rotation;

	/*
	
	spriteData.x += frameTime * velocity.x;
	spriteData.y += frameTime * velocity.y;

	if( spriteData.x > GAME_WIDTH - shipNS::WIDTH * getScale() )
	{
		spriteData.x = GAME_WIDTH - shipNS::WIDTH * getScale();
		velocity.x = -velocity.x;  // reverse X direction
	}
	else if( spriteData.x < 0 )	
	{
		spriteData.x = 0;
		velocity.x = -velocity.x; // reverse X direction
	}

	if( spriteData.y > GAME_HEIGHT - shipNS::HEIGHT * getScale() )
	{
		spriteData.y = GAME_HEIGHT - shipNS::HEIGHT * getScale();
		velocity.y = -velocity.y;  // reverse Y direction
	}
	else if( spriteData.y < 0 )
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;  // reverse Y direction
	}
	*/

}

void Ship::damage( WEAPON weapon )
{
	if( weapon == PLANET ) 
	{
		health -= 10;
	}
	else if( weapon == SHIP )
	{
		health -= 5;
	}
	shieldOn = true;
}

void Ship::explode()
{
	explosionOn = true;
}

void Ship::repair()
{
	active = true;
	health = shipNS::FULL_HEALTH;
	explosionOn = false;
	engineOn = false;
	shieldOn = false;
	rotation = 0.0f;
	direction = shipNS::NONE;
	visible = true;
}