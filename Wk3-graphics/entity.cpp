
#include "entity.h"

Entity::Entity()
{
	radius = 1.0;
	mass = 1.0;
	velocity.x = 0;
	velocity.y = 0;
	deltaV.x = 0;
	deltaV.y = 0;
	active = true;
	collisionType = entityNS::CIRCLE;
	health = 100;
	gravity = entityNS::GRAVITY;
}

bool Entity::initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* tm )
{
	input = gamePtr->getInput();
	audio = gamePtr->getAudio();
	return ( Image::initialize( gamePtr->getGraphics(), width, height, ncols, tm ) );
}

void Entity::update( float frameTime )
{
	velocity += deltaV;
	deltaV.x = 0;
	deltaV.y = 0;

	Image::update( frameTime );
}

void Entity::ai( float frameTime, Entity &ent ) { }

bool Entity::collidesWith( Entity &ent, VECTOR2 &collisionVector )
{
	if( !active || !ent.getActive() ) return false;

	if( collisionType == entityNS::CIRCLE && ent.getCollisionType() == entityNS::CIRCLE )
	{
		return collideCircle( ent, collisionVector);
	}
	if( collisionType == entityNS::BOX && ent.getCollisionType() == entityNS::BOX )
	{
		return collideBox( ent, collisionVector );
	}
	// if neither entities are using circle
	if( collisionType != entityNS::CIRCLE && ent.getCollisionType() != entityNS::CIRCLE )
	{
		return collideRotatedBox( ent, collisionVector );
	}
	else 
	{
		if( collisionType == entityNS::CIRCLE )
		{
			bool collide = ent.collideRotatedBoxCircle( *this, collisionVector );
			collisionVector *= -1;
			return collide;
		}
		else 
		{
			return collideRotatedBoxCircle( ent, collisionVector );
		}
	}
	return false;
}

// returns true when Entity is outside rect
bool Entity::outsideRect( RECT rect )
{
	if( spriteData.x + spriteData.width * getScale() < rect.left || 
		spriteData.x > rect.right ||
		spriteData.y + spriteData.height * getScale() < rect.top ||
		spriteData.y > rect.bottom )
		return true;
	return false;
}

bool Entity::collideCircle( Entity &ent, VECTOR2 &collisionVector )
{
	// difference between centers
	distSquared = *getCenter() - *ent.getCenter();
	distSquared.x = distSquared.x * distSquared.x;
	distSquared.y = distSquared.y * distSquared.y;

	// calculate the sums of the radii
	sumRadiiSquared = (radius * getScale()) + (ent.getRadius() * ent.getScale() );
	sumRadiiSquared *= sumRadiiSquared;
	
	// if entities are colliding
	if( distSquared.x + distSquared.y <= sumRadiiSquared )
	{
		// set the collision vector
						// ent->getCenter() - this->getCenter()
		collisionVector = *ent.getCenter() - *getCenter();
		return true;
	}
	// if not colliding, return false
	return false;
}

bool Entity::collideBox( Entity &ent, VECTOR2 &collisionVector )
{
	// check for collision
	if( (getCenterX() + edge.right*getScale() >= ent.getCenterX() + ent.getEdge().left*ent.getScale() ) &&
		(getCenterX() + edge.left*getScale() <= ent.getCenterX() + ent.getEdge().right*ent.getScale() ) &&
		(getCenterY() + edge.bottom*getScale() >= ent.getCenterY() + ent.getEdge().top*ent.getScale() ) &&
		(getCenterY() + edge.top*getScale() <= ent.getCenterY() + ent.getEdge().bottom*ent.getScale() )	)
	{
		// we have collision! set the collision vector & return true!
		collisionVector = *ent.getCenter() - *getCenter();
		return true;
	}
	return false;
}

bool Entity::collideRotatedBox( Entity &ent, VECTOR2 &collisionVector ) {

	computeRotatedBox();
	ent.computeRotatedBox();
	if( projectionsOverlap( ent ) && ent.projectionsOverlap( *this ) )
	{
		collisionVector = *ent.getCenter() - *getCenter();
		return true;
	}
	return false;
} 

//=============================================================================
// Projects other box onto this edge01 and edge03.
// Called by collideRotatedBox()
// Post: returns true if projections overlap, false otherwise
//=============================================================================
bool Entity::projectionsOverlap(Entity &ent)
{
    float projection, min01, max01, min03, max03;

    // project other box onto edge01
    projection = graphics->Vector2Dot(&edge01, ent.getCorner(0)); // project corner 0
    min01 = projection;
    max01 = projection;
    // for each remaining corner
    for(int c=1; c<4; c++)
    {
        // project corner onto edge01
        projection = graphics->Vector2Dot(&edge01, ent.getCorner(c));
        if (projection < min01)
            min01 = projection;
        else if (projection > max01)
            max01 = projection;
    }
    if (min01 > edge01Max || max01 < edge01Min) // if projections do not overlap
        return false;                       // no collision is possible

    // project other box onto edge03
    projection = graphics->Vector2Dot(&edge03, ent.getCorner(0)); // project corner 0
    min03 = projection;
    max03 = projection;
    // for each remaining corner
    for(int c=1; c<4; c++)
    {
        // project corner onto edge03
        projection = graphics->Vector2Dot(&edge03, ent.getCorner(c));
        if (projection < min03)
            min03 = projection;
        else if (projection > max03)
            max03 = projection;
    }
    if (min03 > edge03Max || max03 < edge03Min) // if projections do not overlap
        return false;                       // no collision is possible

    return true;                            // projections overlap
}

//=============================================================================
// Compute corners of rotated box, projection edges and min and max projections
// 0---1  corner numbers
// |   |
// 3---2
//=============================================================================
void Entity::computeRotatedBox()
{
    if(rotatedBoxReady)
        return;
    float projection;

	// from the current rotation we get two vectors of the entity's x & y
    VECTOR2 rotatedX(cos(spriteData.angle), sin(spriteData.angle));
    VECTOR2 rotatedY(-sin(spriteData.angle), cos(spriteData.angle));

	// center of the entity
    const VECTOR2 *center = getCenter();

	// find the position of the corners of the rotated box
    corners[0] = *center + rotatedX * ((float)edge.left*getScale())  +
                           rotatedY * ((float)edge.top*getScale());
    corners[1] = *center + rotatedX * ((float)edge.right*getScale()) + 
                           rotatedY * ((float)edge.top*getScale());
    corners[2] = *center + rotatedX * ((float)edge.right*getScale()) + 
                           rotatedY * ((float)edge.bottom*getScale());
    corners[3] = *center + rotatedX * ((float)edge.left*getScale())  +
                           rotatedY * ((float)edge.bottom*getScale());

    // corners[0] is used as origin
    // The two edges connected to corners[0] are used as the projection lines
    edge01 = VECTOR2(corners[1].x - corners[0].x, corners[1].y - corners[0].y);
    graphics->Vector2Normalize(&edge01);
    edge03 = VECTOR2(corners[3].x - corners[0].x, corners[3].y - corners[0].y);
    graphics->Vector2Normalize(&edge03);

    // this entities min and max projection onto edges
    projection = graphics->Vector2Dot(&edge01, &corners[0]);
    edge01Min = projection;
    edge01Max = projection;
    // project onto edge01
    projection = graphics->Vector2Dot(&edge01, &corners[1]);
    if (projection < edge01Min)
        edge01Min = projection;
    else if (projection > edge01Max)
        edge01Max = projection;
    // project onto edge03
    projection = graphics->Vector2Dot(&edge03, &corners[0]);
    edge03Min = projection;
    edge03Max = projection;
    projection = graphics->Vector2Dot(&edge03, &corners[3]);
    if (projection < edge03Min)
        edge03Min = projection;
    else if (projection > edge03Max)
        edge03Max = projection;

    rotatedBoxReady = true;
}

bool Entity::collideRotatedBoxCircle( Entity &ent, VECTOR2 &collisionVector )
{
	float min01, min03, max01, max03, center01, center03;

	computeRotatedBox();

	// project our circle onto edge01
	center01 = graphics->Vector2Dot( &edge01, ent.getCenter() );
	min01 = center01 - ent.getRadius()*ent.getScale();
	max01 = center01 + ent.getRadius()*ent.getScale();
	//if the projections do not overlap, no collision is possible 
	if( min01 > edge01Max || max01 < edge01Min ) {
		return false;
	}

	// project our circle onto edge03
	center03 = graphics->Vector2Dot( &edge03, ent.getCenter() );
	min03 = center03 - ent.getRadius()*ent.getScale();
	max03 = center03 + ent.getRadius()*ent.getScale();

	if( min03 > edge03Max || max03 < edge03Min ) {
		return false;
	}

	// check if the circle is in one of the voronoi regions (at the corners)
	if( center01 < edge01Min && center03 < edge03Min )
		return collideCornerCircle( corners[0], ent, collisionVector);
	if( center01 > edge01Max && center03 < edge03Min )
		return collideCornerCircle( corners[1], ent, collisionVector);
	if( center01 > edge01Max && center03 > edge03Max )
		return collideCornerCircle( corners[2], ent, collisionVector);
	if( center01 < edge01Min && center03 > edge03Max )
		return collideCornerCircle( corners[3], ent, collisionVector);

	// we must have collided then!
	collisionVector = *ent.getCenter() - *getCenter();
	return true;
}

bool Entity::collideCornerCircle( VECTOR2 corner, Entity &ent, VECTOR2 &collisionVector )
{
	distSquared = corner - *ent.getCenter(); // corner - circle
	distSquared.x *= distSquared.x;
	distSquared.y *= distSquared.y;

	sumRadiiSquared = ent.getRadius() *ent.getScale();
	sumRadiiSquared *= sumRadiiSquared;

	if( distSquared.x + distSquared.y <= sumRadiiSquared)
	{
		collisionVector = *ent.getCenter() - corner;
		return true;
	}
	return false;
}


void Entity::damage( int weapon ) {
	health -= weapon;
}

void Entity::bounce( VECTOR2 &collisionVector, Entity &ent ) 
{
	VECTOR2 Vdiff = ent.getVelocity() - velocity;
	VECTOR2 cUV = collisionVector;
	Graphics::Vector2Normalize(&cUV);

	float cUVdotVdiff = Graphics::Vector2Dot( &cUV, &Vdiff);
	float massRatio = 2.0f;
	if( getMass() != 0 ) {
		massRatio *= (ent.getMass() / ( getMass() + ent.getMass() ) );
	}

	if( cUVdotVdiff > 0 ) {
		setX( getX() - cUV.x * massRatio);
		setY( getY() - cUV.y * massRatio);
	} else {
		deltaV += ((massRatio * cUVdotVdiff) * cUV);
	}
} 

void Entity::gravityForce( Entity* other, float frameTime ) {}

