// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 4 spacewar.h v1.0

#ifndef _SPACEWAR_H             // prevent multiple definitions if this 
#define _SPACEWAR_H             // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "TextureManager.h"
#include "image.h"
#include "ship.h"
#include "planet.h"

namespace SpacewarNS
{
	const char NEBULA_IMAGE[] = "pictures\\orion.jpg";
	const char SPACE_IMAGE[] = "pictures\\space.jpg";
	//const char PLANET_IMAGE[] = "pictures\\planet.png";
	//const char SHIP_IMAGE[] = "pictures\\ship.png";
	const char GAME_TEXTURES[] = "pictures\\textures.png";

	//const int SHIP_START_FRAME = 0;
	//const int SHIP_END_FRAME = 3;
	//const float SHIP_ANIMATION_DELAY = 0.2f;  // delay between frames for our ship
	//const int SHIP_COLS = 2;
	//const int SHIP_WIDTH = 32;
	//const int SHIP_HEIGHT = 32;
	//const float SHIP_SPEED = 100.0f;
}

//=============================================================================
// Create game class
//=============================================================================
class Spacewar : public Game
{
private:
    // variables
	//TextureManager nebulaTexture;  // handles the texture file
	//TextureManager planetTexture;
	//TextureManager shipTexture;
	TextureManager spaceTexture;
	TextureManager gameTextures; // the spritesheet
	Planet planet;  // planet image, that will be drawn
	//Image nebula;
	Image space;
	Ship ship;
	Ship ship2;

public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
