
#ifndef _SHIP_H
#define _SHIP_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace shipNS
{
	const int WIDTH = 32;
	const int HEIGHT = 32;
	const int X = GAME_WIDTH/2 - WIDTH/2;
	const int Y = GAME_HEIGHT/6 - HEIGHT;
	const float ROTATION_RATE = (float) PI;
	const float SPEED = 100;
	const float MASS = 300.0;
	const int TEXTURE_COLS = 8;
	const int SHIP1_START_FRAME = 0;
	const int SHIP1_END_FRAME = 3; // frames 0, 1, 2, 3
	const int SHIP2_START_FRAME = 8;
	const int SHIP2_END_FRAME = 11; // frames 8, 9, 10, 11
	const float SHIP_ANIMATION_DELAY = 0.5f;

	const int SHIELD_START_FRAME = 24;
	const int SHIELD_END_FRAME = 27; // frames 24, 25, 26, 27
	const float SHIELD_ANIMATION_DELAY = 0.5f;

	enum DIRECTION { NONE, LEFT, RIGHT };
	const int	EXPLOSION_START_FRAME = 32;
	const int	EXPLOSION_END_FRAME = 39;
	const float EXPLOSION_ANIMATION_DELAY = 0.2f;

	const int ENGINE_START_FRAME = 16;
	const int ENGINE_END_FRAME = 19;
	const float ENGINE_ANIMATION_DELAY = 0.2f;

	const float TORPEDO_DAMAGE = 46;
	const float SHIP_DAMAGE = 10;
	const float FULL_HEALTH = 100.0f;
}

class Ship : public Entity
{
private:

	float oldX, oldY, oldAngle;
	float rotation;
	shipNS::DIRECTION direction;
	float explosionTimer;

	bool explosionOn;
	Image explosion;
	bool engineOn;
	Image engine;
	bool shieldOn;
	Image shield;

	bool playingSound;
	float soundTimer;

public:
	Ship();

	virtual void draw();
	virtual bool initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* tm );

	void update( float frameTime );
	void damage( WEAPON );

	void toOldPosition()
	{
		spriteData.x = oldX;
		spriteData.y = oldY;
		spriteData.angle = oldAngle;
		rotation = 0.0;
	}
	
	float getRotation() { return rotation; }
	bool isEngineOn() { return engineOn; }
	bool isShieldOn() { return shieldOn; }

	void setEngine( bool eng ) { engineOn = eng; }
	void setShield( bool sh ) { shieldOn = sh; }
	void setMass( float m ) { mass = m; }
	void setRotation( float r) { rotation = r; }

	void rotate( shipNS::DIRECTION d ) { direction = d; }
	void explode();
	void repair();
};

#endif