// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Game Engine Part 1
// Chapter 4 spacewar.cpp v1.0
// Spacewar is the class we create.

#include "spaceWar.h"

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError

	// initialize the textures
	if( !spaceTexture.initialize( graphics, SpacewarNS::SPACE_IMAGE ) ){
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing space texture"));
	}
	if( !gameTextures.initialize( graphics, SpacewarNS::GAME_TEXTURES ) ) {
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing ship texture"));
	}

	// initialize the images
	if( !space.initialize( graphics, 0, 0, 0, &spaceTexture ) ) {
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing nebula!"));
	}

	space.setX( 0 );
	space.setY( 0 );

	space.setScale( (float)SPACE_SCALE);
	/*
	if( !planet.initialize( this, planetNS::WIDTH, planetNS::HEIGHT, 2, &gameTextures ) ) {
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing planet!"));
	}
	planet.setX( GAME_WIDTH*0.5f - planet.getWidth() *0.5f );
	planet.setY( GAME_HEIGHT*0.5f - planet.getHeight() * 0.5f);
	*/
	if( !ship.initialize( this, 
		shipNS::WIDTH, shipNS::HEIGHT, 
		shipNS::TEXTURE_COLS, &gameTextures ) ) {
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing ship!"));
	}
	ship.setX( GAME_WIDTH/2 - shipNS::WIDTH/2 );
	ship.setY( GAME_HEIGHT/2 - shipNS::HEIGHT/2 );
	ship.setDegrees( 0 );

	ship.setFrames( shipNS::SHIP1_START_FRAME, shipNS::SHIP1_END_FRAME );
	ship.setCurrentFrame( shipNS::SHIP1_START_FRAME );
	ship.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );
	
	ship.setLoop( true );
	//ship.setVelocity( VECTOR2( shipNS::SPEED, -shipNS::SPEED ) );
	//ship.setDegrees( 45.0f );

	/*
	if( !ship2.initialize( this, 
		shipNS::WIDTH, shipNS::HEIGHT, 
		shipNS::TEXTURE_COLS, &gameTextures ) ) {
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing ship!"));
	}
	ship2.setFrames( shipNS::SHIP2_START_FRAME, shipNS::SHIP2_END_FRAME );
	ship2.setCurrentFrame( shipNS::SHIP2_START_FRAME );
	ship2.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );

	ship2.setX( GAME_WIDTH - GAME_WIDTH/4 );
	ship2.setY( GAME_HEIGHT/4 );
	ship2.setLoop( true );
	ship2.setVelocity( VECTOR2( -shipNS::SPEED, -shipNS::SPEED ) );
	*/
    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{

	if( ship.getActive() ) {
		if( input->isKeyDown(UP_KEY) )
		{
			ship.setEngine( true );
		}
		else {
			ship.setEngine( false );
		}
		ship.rotate( shipNS::NONE );
		if( input->isKeyDown(RIGHT_KEY) )  {
			ship.rotate( shipNS::RIGHT );
		}
		if( input->isKeyDown(LEFT_KEY) )  {
			ship.rotate( shipNS::LEFT );
		}

	}

	// update the entities
	ship.update( frameTime );
	//ship2.update( frameTime );
	//planet.update( frameTime );

	// move space in X direction opposite ship
	space.setX( space.getX() - frameTime * ship.getVelocity().x);
	// move space in Y direction opposite ship
	space.setY( space.getY() - frameTime * ship.getVelocity().y);

	// wrap space image around at edge
	// if left edge of space > screen left edge
	if( space.getX() > 0 )	
		space.setX( space.getX() - SPACE_WIDTH);
	if( space.getX() < -SPACE_WIDTH )	
		space.setX( space.getX() + SPACE_WIDTH);
	
	if( space.getY() > 0 )	
		space.setY( space.getY() - SPACE_HEIGHT);
	if( space.getY() < -SPACE_HEIGHT )	
		space.setY( space.getY() + SPACE_HEIGHT);
	/*
	if( input->isKeyDown(RIGHT_KEY) ) 
	{
		ship.setX( ship.getX() + frameTime * SpacewarNS::SHIP_SPEED );
		if( ship.getX() > GAME_WIDTH )
		{
			ship.setX((float)-ship.getWidth());
		}
	}
	if( input->isKeyDown(LEFT_KEY) ) 
	{
		ship.setX( ship.getX() - frameTime * SpacewarNS::SHIP_SPEED );
		if( ship.getX() < -ship.getWidth() )
		{
			ship.setX( (float)GAME_WIDTH);
		}
	}
	if( input->isKeyDown(UP_KEY) ) 
	{
		ship.setY( ship.getY() - frameTime * SpacewarNS::SHIP_SPEED );
		if( ship.getY() < -ship.getHeight() ) 
		{
			ship.setY( (float) GAME_HEIGHT );
		}
	}
	if( input->isKeyDown(DOWN_KEY) ) 
	{
		ship.setY( ship.getY() + frameTime * SpacewarNS::SHIP_SPEED );
		if( ship.getY() > GAME_HEIGHT ) 
		{
			ship.setY( (float) -ship.getHeight() );
		}
	}
	*/
	

	/*
	ship.setDegrees( ship.getDegrees() + frameTime * 180.0f);
	ship.setScale( ship.getScale() + frameTime * 0.2f );
	ship.setX( ship.getX() + frameTime * 100.0f );

	if( ship.getX() > GAME_WIDTH ) {
		ship.setX( (float) -ship.getWidth() ); // positioned offscreen left
		ship.setScale( 1.5f );
	}
	*/
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{
	/*
	VECTOR2 collisionVector;

	if( ship.collidesWith( planet, collisionVector ) )
	{
		ship.bounce( collisionVector, planet );
		ship.damage( PLANET );
	}
	if( ship2.collidesWith( planet, collisionVector ) )
	{
		ship2.bounce( collisionVector, planet );
		ship2.damage( PLANET );
	}
	if( ship.collidesWith( ship2, collisionVector ) )
	{
		ship.bounce( collisionVector, ship2 );
		ship.damage( SHIP );

		ship2.bounce( collisionVector*-1, ship );
		ship2.damage( SHIP );
	} */


}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
	float x = space.getX();
	float y = space.getY();

	graphics->spriteBegin();  // start drawing sprites
	// put draw calls inbetween!
	// the order is important!!
	//nebula.draw();
	space.draw();
	
	if( space.getX() < -SPACE_WIDTH + (int)GAME_WIDTH )
	{
		space.setX( space.getX() + SPACE_WIDTH);
		space.draw();
	}
	if( space.getY() < -SPACE_HEIGHT + (int)GAME_HEIGHT )
	{
		space.setY( space.getY() + SPACE_HEIGHT);
		space.draw();
		space.setX(x);

		if( x < -SPACE_WIDTH + (int)GAME_WIDTH )
			space.draw();

	}
	space.setX(x);
	space.setY(y);

	//planet.draw();
	ship.draw();

	//ship2.draw();
	//------------------
	graphics->spriteEnd(); // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
	// for each TextureManager you have, call onLostDevice() here!
	spaceTexture.onLostDevice();
	gameTextures.onLostDevice();
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
	// for each textureManager object you have, call onResetDevice() here!!
	spaceTexture.onResetDevice();
	gameTextures.onResetDevice();
    Game::resetAll();
    return;
}
