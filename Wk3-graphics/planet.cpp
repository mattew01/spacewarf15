#include "planet.h"


Planet::Planet()
{
	spriteData.width = planetNS::WIDTH;
	spriteData.height = planetNS::HEIGHT;
	spriteData.x = planetNS::X;
	spriteData.y = planetNS::Y;
	spriteData.rect.bottom = planetNS::HEIGHT;
	spriteData.rect.right = planetNS::WIDTH;
	startFrame = planetNS::START_FRAME;
	endFrame = planetNS::END_FRAME;
	currentFrame = startFrame;
	radius = planetNS::COLLISION_RADIUS;
	collisionType = entityNS::CIRCLE;
	mass = planetNS::MASS;
}