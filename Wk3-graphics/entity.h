
#ifndef _ENTITY_H
#define _ENTITY_H
#define WIN32_LEAN_AND_MEAN

#include "image.h"
#include "input.h"
#include "game.h"
#include "audio.h"

namespace entityNS
{
	enum COLLISION_TYPE { NONE, CIRCLE, BOX, ROTATED_BOX };
	const float GRAVITY = 6.67428e-11f;
}

class Entity : public Image
{
protected:
	entityNS::COLLISION_TYPE collisionType;
	VECTOR2 center;		// center of the entity
	float radius;
	VECTOR2 distSquared; // used for circle collision
	float sumRadiiSquared;
	RECT edge;
	VECTOR2 corners[4];		// for ROTATED_BOX
	VECTOR2 edge01;
	VECTOR2 edge03;
	float edge01Min;
	float edge01Max;
	float edge03Min;
	float edge03Max;

	VECTOR2 velocity;		// velocity 
	VECTOR2 deltaV;			// added to velocity during net call to update()
	float mass;
	float health;
	float rr;				// radius squared
	float force;			// force of gravity
	float gravity;			// gravitational constant of the game universe
	
	Input* input;	
	Audio* audio;
	HRESULT hr;
	bool active;
	bool rotatedBoxReady;	// true when the rotatedBox is ready

	virtual bool collideCircle( Entity &ent, VECTOR2 &collisionVector );
	virtual bool collideBox( Entity &ent, VECTOR2 &collisionVector );
	virtual bool collideRotatedBox( Entity &ent, VECTOR2 &collisionVector );
	virtual bool collideRotatedBoxCircle( Entity &ent, VECTOR2 &collisionVector );

	void computeRotatedBox();
	bool projectionsOverlap( Entity &ent );
	bool collideCornerCircle( VECTOR2 corner, Entity &ent, VECTOR2 &collisionVector );

public:
	Entity();
	virtual ~Entity() { }

	///////////////////
	// GET FUNCTIONS //
	///////////////////
	virtual const VECTOR2* getCenter()
	{
		center = VECTOR2( getCenterX(), getCenterY() );
		return &center;
	}
	virtual float	getRadius() const			{ return radius; }
	virtual const	VECTOR2 getVelocity() const	{ return velocity; }
	virtual bool	getActive() const			{ return active; }
	virtual float	getMass() const				{ return mass; }
	virtual float	getGravity() const			{ return gravity; }
	virtual float   getHealth() const			{ return health; }
	virtual entityNS::COLLISION_TYPE getCollisionType() { return collisionType; }
	virtual const RECT& getEdge() const			{ return edge; }
	virtual const VECTOR2* getCorner( UINT c ) const 
	{
		if( c >= 4 ) c = 0;
		return &corners[c];
	}
	/////////////////////////
	//    SET FUNCTIONS    //
	/////////////////////////
	virtual void setVelocity( VECTOR2 v )		{ velocity = v; }
	virtual void setDeltaV( VECTOR2 dv)			{ deltaV = dv; }
	virtual void setActive( bool b )			{ active = b; }
	virtual void setHealth( float h )			{ health = h; }
	virtual void setMass( float m )				{ mass = m; }
	virtual void setGravity( float g )			{ gravity = g; }
	virtual void setCollisionRadius( float r )  { radius = r; }
	//////////////////////////
	//   OTHER FUNCTIONS    //
	//////////////////////////

	virtual void update( float frameTime );

	virtual bool initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* tm );

	virtual void ai( float frameTime, Entity& ent );

	// is this entity outside of the specified rectangle?
	virtual bool outsideRect( RECT rect );

	// does this entity collide with &ent? 
	virtual bool collidesWith( Entity &ent, VECTOR2 &collisionVector );

	virtual void damage( int weapon );

	void bounce( VECTOR2 &collisionVector, Entity &ent );

	void gravityForce( Entity* other, float frameTime );
};

#endif